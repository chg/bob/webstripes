# webSTRIPES

This project provides some code to run the spectro-temporal ripple for investigating processor effectiveness (STRIPES) test as an online experiment in a web browser.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.developers.cam.ac.uk/chg/bob/webstripes/-/blob/main/LICENSE.md)


## Prerequisites

The webSTRIPES experiment was designed to run on a webserver using [JATOS](https://www.jatos.org) and [jsPsych](https://www.jspsych.org).
Before runing the experiment, make sure you have a server or a computer with JATOS installed. (https://www.jatos.org/JATOS-on-a-server.html) 

## Dependencies

The webSTRIPES experiment code is built relying on the [jsPsych](https://www.jspsych.org/6.3/) JavaScript framework. It is included as a submodule in this repository, just clone it with the command below to get started.

## How to do I get set up?

Clone this repository including the [jsPsych dependency](https://github.com/jspsych/jsPsych/tree/v6.3.0) to your computer that runs JATOS:

Using SSH:

```bash
git clone --recurse-submodules git@gitlab.developers.cam.ac.uk:chg/bob/webstripes.git
```

Using HTTPS:

```bash
git clone --recurse-submodules https://gitlab.developers.cam.ac.uk/chg/bob/webstripes.git
```

## Creating the JATOS study archive (JZIP)

In order to import this study in your JATOS install, you first need to create a .jzip file (regular .zip file with .jzip extension) with the following structure:

```
/
├── stripes
│   ├── audio
│   ├── css
│   ├── img
│   ├── jspsych-6.3.0
│   └── stripes.html  
└── stripes.jas
```

An example .jas file is included in this repository with the study properties.

More information on the JATOS study archive:
https://www.jatos.org/JATOS-Study-Archive-JZIP.html

## Try the experiment online

If you do not want to set-up the experiment on your own server, you can still try it in your browser following [this link](https://lsr-studies-02.mrc-cbu.cam.ac.uk/publix/zgUz5wMrg2a).


## Citing webSTRIPES

If you are using webSTRIPES in your work cite us using this:

```BibTex
@article{archer2023online,
  title={An online implementation of a measure of spectro-temporal processing by cochlear-implant listeners},
  author={Archer-Boyd, Alan W and Harland, Andrew and Goehring, Tobias and Carlyon, Robert P},
  journal={JASA Express Letters},
  volume={3},
  number={1},
  pages={014402},
  year={2023},
  publisher={Acoustical Society of America}
}
```

## Contact
For any question related to this code, please contact Bob Carlyon (bob.carlyon@mrc-cbu.cam.ac.uk) or Tobias Goehring (tobias.goehring@mrc-cbu.cam.ac.uk)
